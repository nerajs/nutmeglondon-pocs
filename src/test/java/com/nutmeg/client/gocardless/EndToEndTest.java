package com.nutmeg.client.gocardless;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.Test;

import com.gocardless.GoCardlessClient;
import com.nutmeg.client.gocardless.CustomerBankAccountClient;
import com.nutmeg.client.gocardless.CustomerClient;
import com.nutmeg.client.gocardless.MandateClient;
import com.nutmeg.client.gocardless.PaymentClient;
import com.nutmeg.client.gocardless.SubscriptionClient;
import com.nutmeg.client.gocardless.domain.DirectDebitCustomer;
import com.nutmeg.client.gocardless.domain.DirectDebitCustomerBankAccount;
import com.nutmeg.client.gocardless.domain.DirectDebitMandate;
import com.nutmeg.client.gocardless.domain.DirectDebitPayment;
import com.nutmeg.client.gocardless.domain.DirectDebitSubscription;
import com.nutmeg.client.gocardless.mapper.CustomerBankAccountMapper;
import com.nutmeg.client.gocardless.mapper.CustomerMapper;
import com.nutmeg.client.gocardless.mapper.MandateMapper;
import com.nutmeg.client.gocardless.mapper.PaymentMapper;
import com.nutmeg.client.gocardless.mapper.SubscriptionMapper;

/**
 * There is a fundamental flaw in these tests
 * The tests should not hit the back end system. In fact the backend calls need to be mocked
 * 
 * @author Neraj
 *
 */
public class EndToEndTest {

	private static final String PAYMENT_ID = "PM0000FBSK22BS";
	private static final String SUBSCRIPTION_ID = "SB000012055B8R";
	private static final String CREDITOR_ID = "CR000038N35C0C";
	private static final String SCHEME = "bacs";
	private static final String MANDATE_ID = "MD00007BRGM0GG";
	private static final String BANK_ACCOUNT_ID = "BA000075VZD299";
	private static final String CUSTOMER_ID = "CU00008HQJPD1C";
	
	private GoCardlessClient client;
	
	@Before
	public void setup() {
		
		//used gocardless web based management system to create various entities and which are acting as test data

		client = GoCardlessClient.create("L-_DNa-nUFbBKLPGiIxlMBX52t2JgLjrnlgb7wB4","https://api-sandbox.gocardless.com/");

	}
	
	
	
	@Test
	public void shouldRetrieveCustomer() {
		//get a customer		
		CustomerClient customerClient = new CustomerClient(client, new CustomerMapper());
		DirectDebitCustomer returnedCustomer = customerClient.getById(CUSTOMER_ID);
		
		System.out.println(returnedCustomer);
		
		assertEquals(CUSTOMER_ID, returnedCustomer.getId());
		assertEquals("a street", returnedCustomer.getAddressLine1());
		assertEquals("a locality", returnedCustomer.getAddressLine2());
		assertEquals(null, returnedCustomer.getAddressLine3());
		assertEquals("London", returnedCustomer.getCity());
		assertEquals("GB", returnedCustomer.getCountryCode());
		assertEquals("a@a.a", returnedCustomer.getEmail());

		assertEquals("A", returnedCustomer.getFamilyName());
		assertEquals("A", returnedCustomer.getGivenName());
		assertEquals("W6 9PE", returnedCustomer.getPostalCode());
		assertEquals(null, returnedCustomer.getRegion());
	}
	
	@Test
	public void shouldRetrieveCustomerBankAccount() {
		
		//get customer bank account
		CustomerBankAccountClient customerBankAccountClient = new CustomerBankAccountClient(client, new CustomerBankAccountMapper());
		DirectDebitCustomerBankAccount returnedDirectDebitCustomerBankAccount = customerBankAccountClient.getById(BANK_ACCOUNT_ID);

		assertEquals(BANK_ACCOUNT_ID, returnedDirectDebitCustomerBankAccount.getId());
		assertEquals("NATIONAL WESTMINSTER BANK PLC", returnedDirectDebitCustomerBankAccount.getBankName());
		assertEquals("GBP", returnedDirectDebitCustomerBankAccount.getCurrency());
		assertEquals("GB", returnedDirectDebitCustomerBankAccount.getCountryCode());
		assertEquals(true, returnedDirectDebitCustomerBankAccount.getEnabled());		
	}
	
	
	@Test
	public void shouldCreateMandate() {
		
		//CREATE A DIRECT DEBIT MANDATE
		System.out.println("Before");

		DirectDebitMandate directDebitMandate = new DirectDebitMandate(SCHEME, CREDITOR_ID, BANK_ACCOUNT_ID);
		
		MandateClient mandateClient = new MandateClient(client, new MandateMapper());
		
		DirectDebitMandate createdMandate = mandateClient.create(directDebitMandate);
		
		assertNotNull(createdMandate);
		assertNotNull(createdMandate.getId());

	}
	
	
	@Test
	public void shouldRetrieveMandate() {
		
		MandateClient mandateClient = new MandateClient(client, new MandateMapper());

		//get a mandate
		DirectDebitMandate retrievedMandate = mandateClient.getById(MANDATE_ID);
		
		assertEquals(MANDATE_ID, retrievedMandate.getId());
		assertEquals(null, retrievedMandate.getReference());
		assertEquals("bacs", retrievedMandate.getScheme());
		assertEquals("CR000038N35C0C", retrievedMandate.getCreditor());
		assertEquals("BA00007A4YMQNQ", retrievedMandate.getCustomerBankAccount());		
		assertEquals(null, retrievedMandate.getStatus());
	}
	
	
	@Test
	public void shouldRetrieveSubscription() {
		
		//get a Subscription
		SubscriptionClient subscriptionClient = new SubscriptionClient(client, new SubscriptionMapper());

		DirectDebitSubscription retrievedSubscription = subscriptionClient.getById(SUBSCRIPTION_ID);
		
		assertEquals(SUBSCRIPTION_ID, retrievedSubscription.getId());
		assertEquals(null, retrievedSubscription.getCount());
		assertEquals(new Integer(10000), retrievedSubscription.getAmount());
		assertEquals("GBP", retrievedSubscription.getCurrency());
		assertEquals(new Integer(7), retrievedSubscription.getDayOfMonth());	
		assertEquals("Gold plan", retrievedSubscription.getName());	

		assertEquals("active", retrievedSubscription.getStatus());
	}
	
	
	@Test
	public void shouldCreateSubscription() {
		//lets create a subscription
		//Subscription createdSubscription = client.subscriptions().create().withLinksMandate("MD00007BXKB8PK").withAmount(10000).withCount(5).withIntervalUnit(com.gocardless.services.SubscriptionService.SubscriptionCreateRequest.IntervalUnit.MONTHLY).withCurrency("GBP").withDayOfMonth(7).withName("Gold plan").execute();
		//System.out.println("createdSubscription:" + createdSubscription.toString());
	}

	
	
	@Test
	public void shouldRetrievePayment() {
		
		//get a already created payment

		PaymentClient paymentClient = new PaymentClient(client, new PaymentMapper());
		DirectDebitPayment retrievedPayment = paymentClient.getById(PAYMENT_ID);
		
		assertEquals(PAYMENT_ID, retrievedPayment.getId());
		assertEquals(new Integer(10000), retrievedPayment.getAmount());
		assertEquals("GBP", retrievedPayment.getCurrency());
		assertEquals("may", retrievedPayment.getDescription());	
		assertEquals(null, retrievedPayment.getReference());	
	}

	
	
	
	
	// FAILURE SCENARIO TESTS HERE
	
	
}
