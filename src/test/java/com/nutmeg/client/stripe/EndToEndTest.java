package com.nutmeg.client.stripe;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import com.nutmeg.client.stripe.testdatabuilder.PlanBuilder;
import com.nutmeg.client.stripe.testdatabuilder.TokenBuilder;
import com.stripe.Stripe;
import com.stripe.exception.APIConnectionException;
import com.stripe.exception.APIException;
import com.stripe.exception.AuthenticationException;
import com.stripe.exception.CardException;
import com.stripe.exception.InvalidRequestException;
import com.stripe.model.Card;
import com.stripe.model.Customer;
import com.stripe.model.Subscription;
import static com.nutmeg.client.stripe.testdatabuilder.ChargeBuilder.createCharge;



public class EndToEndTest {

	@Before
	public void setup() {
		// This will take care of the Authentication
		Stripe.apiKey = "sk_test_daG2ZrB9ioJfnmEFLCbKUhQb";
	}
	
	
	@Test
	public void shouldWorkEndToEnd() throws AuthenticationException,
			InvalidRequestException, APIConnectionException, CardException,
			APIException {
		Customer c = createCustomer();
		
		Card card = createCard(c);
		
		Subscription s = createSubscription(c);
		
		createCharge(true);
		
	}

	private Customer createCustomer() throws AuthenticationException,
			InvalidRequestException, APIConnectionException, CardException,
			APIException {
		Map<String, Object> customerParams = new HashMap<String, Object>();
		customerParams.put("description", "Customer for test@example.com");
		customerParams.put("source", TokenBuilder.buildCardToken().getId());

		return Customer.create(customerParams);
	}


	private Card createCard(Customer cu) throws AuthenticationException,
			InvalidRequestException, APIConnectionException, CardException,
			APIException {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("source", TokenBuilder.buildCardToken().getId());
		return cu.createCard(params);
	}

	private Subscription createSubscription(Customer cu) throws AuthenticationException,
			InvalidRequestException, APIConnectionException, CardException,
			APIException {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("plan", PlanBuilder.createPlan().getId());
		return cu.createSubscription(params);
	}

}
