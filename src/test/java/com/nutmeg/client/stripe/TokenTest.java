package com.nutmeg.client.stripe;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import com.nutmeg.client.stripe.testdatabuilder.TokenBuilder;
import com.stripe.Stripe;
import com.stripe.exception.APIConnectionException;
import com.stripe.exception.APIException;
import com.stripe.exception.AuthenticationException;
import com.stripe.exception.CardException;
import com.stripe.exception.InvalidRequestException;
import com.stripe.model.Token;

public class TokenTest {

	@Before
	public void setup() {
		//This will take care of the Authentication
		Stripe.apiKey = "sk_test_daG2ZrB9ioJfnmEFLCbKUhQb";
	}


	@Test
	public void shouldCreateAToken() throws AuthenticationException, InvalidRequestException, APIConnectionException, CardException, APIException {
		TokenBuilder.buildCardToken();
	}
	
	@Test
	public void shouldCreateABankAccountToken() throws AuthenticationException, InvalidRequestException, APIConnectionException, CardException, APIException {
		TokenBuilder.buildBankAccountToken();
	}
	
	@Test
	public void shouldRetrieveAToken() throws AuthenticationException, InvalidRequestException, APIConnectionException, CardException, APIException {
	
		Token.retrieve(TokenBuilder.buildCardToken().getId());
	}
	
}

