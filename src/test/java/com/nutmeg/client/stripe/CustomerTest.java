package com.nutmeg.client.stripe;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import com.nutmeg.client.stripe.testdatabuilder.TokenBuilder;
import com.stripe.Stripe;
import com.stripe.exception.APIConnectionException;
import com.stripe.exception.APIException;
import com.stripe.exception.AuthenticationException;
import com.stripe.exception.CardException;
import com.stripe.exception.InvalidRequestException;
import com.stripe.model.Customer;

public class CustomerTest {
	
	
	
	@Before
	public void setup() {
		//This will take care of the Authentication
		Stripe.apiKey = "sk_test_daG2ZrB9ioJfnmEFLCbKUhQb";
	}
	
	@Test
	public void shouldCreateACustomer() throws AuthenticationException, InvalidRequestException, APIConnectionException, CardException, APIException {

		createCustomer();
	}

	
	@Test
	public void shouldRetrieveACustomer() throws AuthenticationException, InvalidRequestException, APIConnectionException, CardException, APIException {
		Customer.retrieve("cus_6H7b5lvt9fpAHK");
	}
	
	
	@Test
	public void shouldUpdateACustomer() throws AuthenticationException, InvalidRequestException, APIConnectionException, CardException, APIException {
		Customer cu = Customer.retrieve("cus_6H7b5lvt9fpAHK");
		Map<String, Object> updateParams = new HashMap<String, Object>();
		updateParams.put("description", "Customer for test@example.com");
	
		cu.update(updateParams);
	}
	
	@Test
	public void shouldDeleteACustomer() throws AuthenticationException, InvalidRequestException, APIConnectionException, CardException, APIException {
		Customer cu = Customer.retrieve(createCustomer().getId());
		cu.delete();
	}
	
	
	@Test
	public void shouldListAllCustomers() throws AuthenticationException, InvalidRequestException, APIConnectionException, CardException, APIException {
		Map<String, Object> customerParams = new HashMap<String, Object>();
		customerParams.put("limit", 3);

		Customer.all(customerParams);
	}
	
	private Customer createCustomer() throws AuthenticationException,
			InvalidRequestException, APIConnectionException, CardException,
			APIException {
		Map<String, Object> customerParams = new HashMap<String, Object>();
		customerParams.put("description", "Customer for test@example.com");
		customerParams.put("source", TokenBuilder.buildCardToken().getId());
	
		return Customer.create(customerParams);
	}
	
	

}
