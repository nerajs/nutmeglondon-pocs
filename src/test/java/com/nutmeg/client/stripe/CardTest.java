package com.nutmeg.client.stripe;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import com.nutmeg.client.stripe.testdatabuilder.TokenBuilder;
import com.stripe.Stripe;
import com.stripe.exception.APIConnectionException;
import com.stripe.exception.APIException;
import com.stripe.exception.AuthenticationException;
import com.stripe.exception.CardException;
import com.stripe.exception.InvalidRequestException;
import com.stripe.model.Card;
import com.stripe.model.Customer;
import com.stripe.model.PaymentSource;

public class CardTest {

	
	@Before
	public void setup() {
		//This will take care of the Authentication
		Stripe.apiKey = "sk_test_daG2ZrB9ioJfnmEFLCbKUhQb";
	}
	
	@Test
	public void shouldCreateACard() throws AuthenticationException, InvalidRequestException, APIConnectionException, CardException, APIException {
		createCard();
	}
	
	
	@Test
	public void shouldRetrieveACard() throws AuthenticationException, InvalidRequestException, APIConnectionException, CardException, APIException {

		Customer cu = Customer.retrieve("cus_6H7b5lvt9fpAHK");
		PaymentSource source = cu.getSources().retrieve("ba_164puYLblSfWSMgI5ELmitu5");
		if (source.getObject().equals("card")) {
		  Card card = (Card) source;
		  // use the card!
		}
	
	}
	
	@Test
	public void shouldUpdateACard() throws AuthenticationException, InvalidRequestException, APIConnectionException, CardException, APIException {
		Customer cu = Customer.retrieve("cus_6H7b5lvt9fpAHK");
		PaymentSource source = cu.getSources().retrieve("ba_164puYLblSfWSMgI5ELmitu5");
		Map<String, Object> updateParams = new HashMap<String, Object>();
		//updateParams.put("exp_year", "2019");
		source.update(updateParams);
	}
	
	
	@Test
	public void shouldDeleteACard() throws AuthenticationException, InvalidRequestException, APIConnectionException, CardException, APIException {
	
		String cardIdToDelete = createCard().getId();
		
		Customer cu = Customer.retrieve("cus_6H7b5lvt9fpAHK");
		for(PaymentSource source : cu.getSources().getData()){
		  if(source.getId().equals(cardIdToDelete)){
		    source.delete();
		    break;
		  }
		}
	}
	
	
	@Test
	public void shouldListAllCards() throws AuthenticationException, InvalidRequestException, APIConnectionException, CardException, APIException {
		Customer cu = Customer.retrieve("cus_6H7b5lvt9fpAHK");
		Map<String, Object> cardParams = new HashMap<String, Object>();
		cardParams.put("limit", 3);
		cardParams.put("object", "card");
		cu.getSources().all(cardParams);
	}
	

	private Card createCard() throws AuthenticationException, InvalidRequestException, APIConnectionException, CardException, APIException {
		Customer cu = Customer.retrieve("cus_6H7b5lvt9fpAHK");
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("source", TokenBuilder.buildCardToken().getId());
		return cu.createCard(params);
	}
	
	
}
