package com.nutmeg.client.stripe.testdatabuilder;

import java.util.HashMap;
import java.util.Map;

import com.stripe.exception.APIConnectionException;
import com.stripe.exception.APIException;
import com.stripe.exception.AuthenticationException;
import com.stripe.exception.CardException;
import com.stripe.exception.InvalidRequestException;
import com.stripe.model.Charge;

public class ChargeBuilder {
	
	public static Charge createCharge(boolean capture) throws AuthenticationException, InvalidRequestException, APIConnectionException, CardException, APIException{
		Map<String, Object> chargeParams = new HashMap<String, Object>();
		chargeParams.put("amount", 400);
		chargeParams.put("currency", "usd");	
		chargeParams.put("source", TokenBuilder.buildCardToken().getId());

		chargeParams.put("capture", String.valueOf(capture));

		chargeParams.put("description", "Charge for test@example.com");

		return Charge.create(chargeParams);
	}

}
