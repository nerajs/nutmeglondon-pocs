package com.nutmeg.client.stripe.testdatabuilder;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import com.stripe.exception.APIConnectionException;
import com.stripe.exception.APIException;
import com.stripe.exception.AuthenticationException;
import com.stripe.exception.CardException;
import com.stripe.exception.InvalidRequestException;
import com.stripe.model.Plan;

public class PlanBuilder {

	public static Plan createPlan()
			throws AuthenticationException, InvalidRequestException,
			APIConnectionException, CardException, APIException {
		
		String planId = "plan" + UUID.randomUUID();
		
		Map<String, Object> planParams = new HashMap<String, Object>();
		planParams.put("amount", 2000);
		planParams.put("interval", "month");
		planParams.put("name", planId);
		planParams.put("currency", "gbp");
		planParams.put("id", planId);

		return Plan.create(planParams);
	}

}
