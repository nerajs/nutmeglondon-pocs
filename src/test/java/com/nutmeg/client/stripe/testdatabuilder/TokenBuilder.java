package com.nutmeg.client.stripe.testdatabuilder;

import java.util.HashMap;
import java.util.Map;

import com.nutmeg.client.stripe.TokenClient;
import com.stripe.exception.APIConnectionException;
import com.stripe.exception.APIException;
import com.stripe.exception.AuthenticationException;
import com.stripe.exception.CardException;
import com.stripe.exception.InvalidRequestException;
import com.stripe.model.Token;

public class TokenBuilder {

	
	public static Token buildCardToken() throws AuthenticationException, InvalidRequestException, APIConnectionException, CardException, APIException{
		Map<String, Object> tokenParams = new HashMap<String, Object>();
		Map<String, Object> cardParams = new HashMap<String, Object>();
		cardParams.put("number", "4242424242424242");
		cardParams.put("exp_month", 5);
		cardParams.put("exp_year", 2016);
		cardParams.put("cvc", "314");
		tokenParams.put("card", cardParams);

		
		TokenClient tokenClient = new TokenClient();
		
		return tokenClient.create(tokenParams);
	}
	
	
	public static Token buildBankAccountToken() throws AuthenticationException, InvalidRequestException, APIConnectionException, CardException, APIException{
		
		Map<String, Object> tokenParams = new HashMap<String, Object>();
		Map<String, Object> bank_accountParams = new HashMap<String, Object>();
		bank_accountParams.put("country", "US");
		bank_accountParams.put("routing_number", "110000000");
		bank_accountParams.put("account_number", "000123456789");
		tokenParams.put("bank_account", bank_accountParams);

		
		TokenClient tokenClient = new TokenClient();
		
		return tokenClient.create(tokenParams);
	}
}
