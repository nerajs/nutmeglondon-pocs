package com.nutmeg.client.stripe;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.junit.Before;
import org.junit.Test;

import com.nutmeg.client.stripe.testdatabuilder.PlanBuilder;
import com.stripe.Stripe;
import com.stripe.exception.APIConnectionException;
import com.stripe.exception.APIException;
import com.stripe.exception.AuthenticationException;
import com.stripe.exception.CardException;
import com.stripe.exception.InvalidRequestException;
import com.stripe.model.Plan;

public class PlanTest {

	
	@Before
	public void setup() {
		//This will take care of the Authentication
		Stripe.apiKey = "sk_test_daG2ZrB9ioJfnmEFLCbKUhQb";
	}
	
	@Test
	public void shouldCreateAPlan() throws AuthenticationException, InvalidRequestException, APIConnectionException, CardException, APIException {
		PlanBuilder.createPlan();
	}

	
	@Test
	public void shouldRetrieveAPlan() throws AuthenticationException, InvalidRequestException, APIConnectionException, CardException, APIException {
		Plan.retrieve(PlanBuilder.createPlan().getId());
	}
	
	@Test
	public void shouldUpdateAPlan() throws AuthenticationException, InvalidRequestException, APIConnectionException, CardException, APIException {
		Plan p = Plan.retrieve(PlanBuilder.createPlan().getId());
		Map<String, Object> updateParams = new HashMap<String, Object>();
		updateParams.put("name", "New plan name");
		p.update(updateParams);
	}
	
	
	@Test
	public void shouldDeleteAPlan() throws AuthenticationException, InvalidRequestException, APIConnectionException, CardException, APIException {
	
		Plan plan = Plan.retrieve(PlanBuilder.createPlan().getId());
		plan.delete();
	}
	
	
	@Test
	public void shouldListAllPlans() throws AuthenticationException, InvalidRequestException, APIConnectionException, CardException, APIException {
		Map<String, Object> planParams = new HashMap<String, Object>();
		planParams.put("limit", 3);

		Plan.all(planParams);
	}
	

	
	
	
}
