package com.nutmeg.client.stripe;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import com.stripe.Stripe;
import com.stripe.exception.APIConnectionException;
import com.stripe.exception.APIException;
import com.stripe.exception.AuthenticationException;
import com.stripe.exception.CardException;
import com.stripe.exception.InvalidRequestException;
import com.stripe.model.Charge;
import com.stripe.model.ChargeRefundCollection;
import com.stripe.model.Refund;

import static com.nutmeg.client.stripe.testdatabuilder.ChargeBuilder.createCharge;


public class RefundTest {
	
	@Before
	public void setup() {
		//This will take care of the Authentication
		Stripe.apiKey = "sk_test_daG2ZrB9ioJfnmEFLCbKUhQb";
	}
	
	@Test
	public void shouldCreateARefund() throws AuthenticationException, InvalidRequestException, APIConnectionException, CardException, APIException {
		createRefund();
	}

	private Refund createRefund() throws AuthenticationException,
			InvalidRequestException, APIConnectionException, CardException,
			APIException {
		Map<String, Object> params = new HashMap<String, Object>();
		Charge ch = createCharge(true);
		return ch.getRefunds().create(params);
	}

	@Test
	public void shouldRetrieveARefund() throws AuthenticationException, InvalidRequestException, APIConnectionException, CardException, APIException {

		Map<String, Object> params = new HashMap<String, Object>();
		Charge ch = createCharge(true);
		String refundId = ch.getRefunds().create(params).getId();
		Refund refund = ch.getRefunds().retrieve(refundId);
	}
	
	@Test
	public void shouldUpdateARefund() throws AuthenticationException, InvalidRequestException, APIConnectionException, CardException, APIException {

		Refund re = createRefund();
		Map<String, String> metadata = new HashMap<String, String>();
		metadata.put("key", "value");
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("metadata", metadata);
		re.update(params);
	}
	
	@Test
	public void shouldListAllRefunds() throws AuthenticationException, InvalidRequestException, APIConnectionException, CardException, APIException {

		Charge ch = createCharge(true);
		Map<String, Object> refundParams = new HashMap<String, Object>();
		refundParams.put("limit", 3);
		ChargeRefundCollection refunds = ch.getRefunds().all(refundParams);
	}
	
	
	
	
}
