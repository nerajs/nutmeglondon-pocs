package com.nutmeg.client.stripe;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import com.nutmeg.client.stripe.testdatabuilder.PlanBuilder;
import com.stripe.Stripe;
import com.stripe.exception.APIConnectionException;
import com.stripe.exception.APIException;
import com.stripe.exception.AuthenticationException;
import com.stripe.exception.CardException;
import com.stripe.exception.InvalidRequestException;
import com.stripe.model.Customer;
import com.stripe.model.CustomerSubscriptionCollection;
import com.stripe.model.Subscription;

public class SubscriptionTest {

	
	@Before
	public void setup() {
		//This will take care of the Authentication
		Stripe.apiKey = "sk_test_daG2ZrB9ioJfnmEFLCbKUhQb";
	}
	
	@Test
	public void shouldCreateASubscription() throws AuthenticationException, InvalidRequestException, APIConnectionException, CardException, APIException {
		createSubscription();
	}

	private Subscription createSubscription() throws AuthenticationException,
			InvalidRequestException, APIConnectionException, CardException,
			APIException {
		Customer cu = Customer.retrieve("cus_6H7b5lvt9fpAHK");
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("plan", PlanBuilder.createPlan().getId());
		return cu.createSubscription(params);
	}
	
	
	@Test
	public void shouldRetrieveASubscription() throws AuthenticationException, InvalidRequestException, APIConnectionException, CardException, APIException {

		Customer cu = Customer.retrieve("cus_6H7b5lvt9fpAHK");
		Subscription su =  cu.getSubscriptions().retrieve(createSubscription().getId());
	
	}
	
	@Test
	public void shouldUpdateASubscription() throws AuthenticationException, InvalidRequestException, APIConnectionException, CardException, APIException {
		Customer cu = Customer.retrieve("cus_6H7b5lvt9fpAHK");
		Subscription subscription = cu.getSubscriptions().retrieve(createSubscription().getId());
		Map<String, Object> updateParams = new HashMap<String, Object>();
		updateParams.put("plan", PlanBuilder.createPlan().getId());
		subscription.update(updateParams);
	}
	
	
	@Test
	public void shouldCancelASubscription() throws AuthenticationException, InvalidRequestException, APIConnectionException, CardException, APIException {
	
		Customer cu = Customer.retrieve("cus_6H7b5lvt9fpAHK");
		for(Subscription subscription : cu.getSubscriptions().getData()){
		  if(subscription.getId().equals("sub_6H5JK4zDbsN7E0")){
		    subscription.cancel(null);
		    break;
		  }
		}
	}
	
	
	@Test
	public void shouldListActiveSubscriptions() throws AuthenticationException, InvalidRequestException, APIConnectionException, CardException, APIException {
		Customer cu = Customer.retrieve("cus_6H7b5lvt9fpAHK");
		Map<String, Object> subscriptionParams = new HashMap<String, Object>();
		subscriptionParams.put("limit", 3);
		CustomerSubscriptionCollection subscriptions = cu.getSubscriptions().all(subscriptionParams);
	}
	
}
