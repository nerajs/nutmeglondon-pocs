package com.nutmeg.client.stripe;

import static com.nutmeg.client.stripe.testdatabuilder.ChargeBuilder.createCharge;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import com.stripe.Stripe;
import com.stripe.exception.APIConnectionException;
import com.stripe.exception.APIException;
import com.stripe.exception.AuthenticationException;
import com.stripe.exception.CardException;
import com.stripe.exception.InvalidRequestException;
import com.stripe.model.Charge;

public class ChargeTest {

	
	@Before
	public void setup() {
		//This will take care of the Authentication
		Stripe.apiKey = "sk_test_daG2ZrB9ioJfnmEFLCbKUhQb";
	}
	
	@Test
	public void shouldCreateACharge() throws AuthenticationException, InvalidRequestException, APIConnectionException, CardException, APIException {
		createCharge(true);
	}
	
	
	
	@Test
	public void shouldRetrieveACharge() throws AuthenticationException, InvalidRequestException, APIConnectionException, CardException, APIException {
		Charge.retrieve(createCharge(true).getId());
	}
	
	
	@Test
	public void shouldUpdateACharge() throws AuthenticationException, InvalidRequestException, APIConnectionException, CardException, APIException {

		Charge ch = createCharge(true);
		Map<String, Object> updateParams = new HashMap<String, Object>();
		updateParams.put("description", "Charge for test@example.com");
	
		ch.update(updateParams);
	}
	
	
	@Test
	public void shouldCaptureACharge() throws AuthenticationException, InvalidRequestException, APIConnectionException, CardException, APIException {
		Charge ch = createCharge(false);
		ch.capture();
	}
	
	@Test
	public void shouldListAllCharges() throws AuthenticationException, InvalidRequestException, APIConnectionException, CardException, APIException {
		Map<String, Object> chargeParams = new HashMap<String, Object>();
		chargeParams.put("limit", 3);

		Charge.all(chargeParams);
	}
		
}
