package com.nutmeg.client.stripe;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.junit.Before;
import org.junit.Test;

import com.stripe.Stripe;
import com.stripe.exception.APIConnectionException;
import com.stripe.exception.APIException;
import com.stripe.exception.AuthenticationException;
import com.stripe.exception.CardException;
import com.stripe.exception.InvalidRequestException;
import com.stripe.model.Coupon;

public class CouponTest {

	
	@Before
	public void setup() {
		//This will take care of the Authentication
		Stripe.apiKey = "sk_test_daG2ZrB9ioJfnmEFLCbKUhQb";
	}
	
	@Test
	public void shouldCreateACoupon() throws AuthenticationException, InvalidRequestException, APIConnectionException, CardException, APIException {
		createCoupon();
	}

	private Coupon createCoupon() throws AuthenticationException,
			InvalidRequestException, APIConnectionException, CardException,
			APIException {
		Map<String, Object> couponParams = new HashMap<String, Object>();
		couponParams.put("percent_off", 25);
		couponParams.put("duration", "repeating");
		couponParams.put("duration_in_months", 3);
		couponParams.put("id", UUID.randomUUID());

		return Coupon.create(couponParams);
	}
	
	
	@Test
	public void shouldRetrieveACoupon() throws AuthenticationException, InvalidRequestException, APIConnectionException, CardException, APIException {
		Coupon cpn =  Coupon.retrieve(createCoupon().getId());
	}
	
	@Test
	public void shouldUpdateACoupon() throws AuthenticationException, InvalidRequestException, APIConnectionException, CardException, APIException {
		Coupon coupon = Coupon.retrieve(createCoupon().getId());
		
		Map metadata = new HashMap();
		metadata.put("key", "value");
		Map params = new HashMap();
		params.put("metadata", metadata);
		coupon.update(params);
	}
	
	
	@Test
	public void shouldDeleteACoupon() throws AuthenticationException, InvalidRequestException, APIConnectionException, CardException, APIException {
	
		Coupon cpn = Coupon.retrieve(createCoupon().getId());
		cpn.delete();
	}
	
	
	@Test
	public void shouldListAllCoupons() throws AuthenticationException, InvalidRequestException, APIConnectionException, CardException, APIException {
		Map<String, Object> couponParams = new HashMap<String, Object>();
		couponParams.put("limit", 3);

		Coupon.all(couponParams);
	}
	
}
