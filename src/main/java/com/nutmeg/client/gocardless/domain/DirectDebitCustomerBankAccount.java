package com.nutmeg.client.gocardless.domain;


public class DirectDebitCustomerBankAccount {
	
	private String id;
	private String bankName;
	private String countryCode;
	private String currency;
	private Boolean enabled;

	

	public DirectDebitCustomerBankAccount(String id, String accountHolderName,
			String bankName, String countryCode, String currency,
			Boolean enabled) {
		
		this.id = id;
		this.bankName = bankName;
		this.countryCode = countryCode;
		this.currency = currency;
		this.enabled = enabled;

	}



	public String getId() {
		return id;
	}



	public String getBankName() {
		return bankName;
	}



	public String getCountryCode() {
		return countryCode;
	}



	public String getCurrency() {
		return currency;
	}



	public Boolean getEnabled() {
		return enabled;
	}



	@Override
	public String toString() {
		return "DirectDebitCustomerBankAccount [id=" + id + ", bankName="
				+ bankName + ", countryCode=" + countryCode + ", currency="
				+ currency + ", enabled=" + enabled + "]";
	}
	
	

}
