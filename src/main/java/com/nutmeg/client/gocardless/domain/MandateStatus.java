package com.nutmeg.client.gocardless.domain;

import com.google.gson.annotations.SerializedName;

public enum MandateStatus {
    @SerializedName("pending_submission")
    PENDING_SUBMISSION, @SerializedName("submitted")
    SUBMITTED, @SerializedName("active")
    ACTIVE, @SerializedName("failed")
    FAILED, @SerializedName("cancelled")
    CANCELLED, @SerializedName("expired")
    EXPIRED;
    @Override
    public String toString() {
        return name().toLowerCase();
    }
}