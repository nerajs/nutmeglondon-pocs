package com.nutmeg.client.gocardless.domain;

import java.util.Map;

public class DirectDebitMandate {
	
	private String id;

    private String reference;
    private String scheme;
    private String creditor;
    private String customerBankAccount;
    private MandateStatus status;
    private Map<String, String> metadata;
    
    
    public DirectDebitMandate(String mandateId, String scheme, String creditor, String bankAccount) {
    	this.id = mandateId;
    	this.scheme = scheme;
    	this.creditor = creditor;
    	this.customerBankAccount = bankAccount;
    }
    
    public DirectDebitMandate(String scheme, String creditor, String bankAccount){
    	this(null, scheme, creditor, bankAccount);
    }
    
	public String getId() {
		return id;
	}

	public String getReference() {
		return reference;
	}
	public void setReference(String reference) {
		this.reference = reference;
	}
	public String getScheme() {
		return scheme;
	}
	public void setScheme(String scheme) {
		this.scheme = scheme;
	}
	public String getCreditor() {
		return creditor;
	}
	public void setCreditor(String creditor) {
		this.creditor = creditor;
	}
	public String getCustomerBankAccount() {
		return customerBankAccount;
	}
	public void setCustomerBankAccount(String customerBankAccount) {
		this.customerBankAccount = customerBankAccount;
	}
	public MandateStatus getStatus() {
		return status;
	}
	public void setStatus(MandateStatus status) {
		this.status = status;
	}
	public Map<String, String> getMetadata() {
		return metadata;
	}
	public void setMetadata(Map<String, String> metadata) {
		this.metadata = metadata;
	}

	@Override
	public String toString() {
		return "DirectDebitMandate [mandateId=" + id + ", reference="
				+ reference + ", scheme=" + scheme + ", creditor=" + creditor
				+ ", customerBankAccount=" + customerBankAccount + ", status="
				+ status + ", metadata=" + metadata + "]";
	}
    
}
