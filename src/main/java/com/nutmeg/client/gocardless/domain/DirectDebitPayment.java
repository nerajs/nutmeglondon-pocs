package com.nutmeg.client.gocardless.domain;


public class DirectDebitPayment {

	private String id;
	private Integer amount;
	private String currency;
	private String description;
	private String reference;
	
	public DirectDebitPayment(String id, Integer amount, String currency, String description, String reference) {
		this.id = id;
		this.amount = amount;
		this.currency = currency;
		this.description = description;
		this.reference = reference;
	}

	public String getId() {
		return id;
	}

	public Integer getAmount() {
		return amount;
	}

	public String getCurrency() {
		return currency;
	}

	public String getDescription() {
		return description;
	}

	public String getReference() {
		return reference;
	}

	@Override
	public String toString() {
		return "DirectDebitPayment [id=" + id + ", amount=" + amount
				+ ", currency=" + currency + ", description=" + description
				+ ", reference=" + reference + "]";
	}

}
