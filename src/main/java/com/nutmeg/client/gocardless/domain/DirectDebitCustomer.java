package com.nutmeg.client.gocardless.domain;

import java.util.Map;

public class DirectDebitCustomer {

	private String id;
    private String addressLine1;
    private String addressLine2;
    private String addressLine3;
    private String city;
    private String countryCode;
    private String email;
    private String familyName;
    private String givenName;
    private Map<String, String> metadata;
    private String postalCode;
    private String region;
    
    public DirectDebitCustomer(String id, String addressLine1, String addressLine2,
    		        String addressLine3, String city, String countryCode,
    		        String email,String familyName, String givenName, Map<String, String> metadata, 
    		        String postalCode, String region ){
    	
    	this.id=id;
    	this.addressLine1=addressLine1;
    	this.addressLine2=addressLine2;
    	this.addressLine3=addressLine3;
    	this.city=city;
    	this.countryCode=countryCode;
    	this.email=email;
    	this.familyName=familyName;
    	this.givenName=givenName;
    	this.metadata=metadata;
    	this.postalCode=postalCode;
    	this.region=region;
    }
    
    
	public String getId() {
		return id;
	}


	public String getAddressLine1() {
		return addressLine1;
	}
	public String getAddressLine2() {
		return addressLine2;
	}
	public String getAddressLine3() {
		return addressLine3;
	}
	public String getCity() {
		return city;
	}
	public String getCountryCode() {
		return countryCode;
	}
	public String getEmail() {
		return email;
	}
	public String getFamilyName() {
		return familyName;
	}
	public String getGivenName() {
		return givenName;
	}
	public Map<String, String> getMetadata() {
		return metadata;
	}
	public String getPostalCode() {
		return postalCode;
	}
	public String getRegion() {
		return region;
	}


	@Override
	public String toString() {
		return "DirectDebitCustomer [id=" + id + ", addressLine1="
				+ addressLine1 + ", addressLine2=" + addressLine2
				+ ", addressLine3=" + addressLine3 + ", city=" + city
				+ ", countryCode=" + countryCode + ", email=" + email
				+ ", familyName=" + familyName + ", givenName=" + givenName
				+ ", metadata=" + metadata + ", postalCode=" + postalCode
				+ ", region=" + region + "]";
	}  
	
}
