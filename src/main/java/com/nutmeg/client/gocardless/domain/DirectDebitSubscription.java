package com.nutmeg.client.gocardless.domain;


public class DirectDebitSubscription {
	
	private Integer amount;
	private Integer count;
	private String currency;
	private Integer dayOfMonth;
	private String id;
	private String name;
	private String status;


	public DirectDebitSubscription(Integer amount, Integer count,
			String currency, Integer dayOfMonth, String id, 
			String name, String status) {

		this.amount = amount;
		this.count = count;
		this.currency = currency;
		this.dayOfMonth = dayOfMonth;
		this.id = id;
		this.name = name;
		this.status = status;
	}


	@Override
	public String toString() {
		return "DirectDebitSubscription [amount=" + amount + ", count=" + count
				+ ", currency=" + currency + ", dayOfMonth=" + dayOfMonth
				+ ", id=" + id + ", name=" + name + ", status=" + status + "]";
	}


	public Integer getAmount() {
		return amount;
	}


	public Integer getCount() {
		return count;
	}


	public String getCurrency() {
		return currency;
	}


	public Integer getDayOfMonth() {
		return dayOfMonth;
	}


	public String getId() {
		return id;
	}


	public String getName() {
		return name;
	}


	public String getStatus() {
		return status;
	}
	
	
	

}
