package com.nutmeg.client.gocardless.mapper;

import com.gocardless.resources.Subscription;
import com.nutmeg.client.gocardless.domain.DirectDebitSubscription;

public class SubscriptionMapper {

	public DirectDebitSubscription convertToDirectDebitSubscription( Subscription subscription) {
		return new DirectDebitSubscription(subscription.getAmount(), subscription.getCount(), subscription.getCurrency(), subscription.getDayOfMonth(), subscription.getId(), subscription.getName(), subscription.getStatus());
	}

}
