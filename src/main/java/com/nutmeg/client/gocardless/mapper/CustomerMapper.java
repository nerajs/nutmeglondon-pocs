package com.nutmeg.client.gocardless.mapper;

import com.gocardless.resources.Customer;
import com.nutmeg.client.gocardless.domain.DirectDebitCustomer;

public class CustomerMapper {

	public DirectDebitCustomer convertToDirectDebitCustomer(
			Customer customer) {
		return new DirectDebitCustomer(customer.getId(), customer.getAddressLine1(), customer.getAddressLine2(),
				customer.getAddressLine3(), customer.getCity(), customer.getCountryCode(),
				customer.getEmail(), customer.getFamilyName(), customer.getGivenName(), customer.getMetadata(), 
				customer.getPostalCode(), customer.getRegion() );
	}

}
