package com.nutmeg.client.gocardless.mapper;

import com.gocardless.resources.CustomerBankAccount;
import com.nutmeg.client.gocardless.domain.DirectDebitCustomerBankAccount;

public class CustomerBankAccountMapper {

	public DirectDebitCustomerBankAccount convertToDirectDebitCustomerBankAccount(
			CustomerBankAccount customerBankAccount) {

		return new DirectDebitCustomerBankAccount(customerBankAccount.getId(), customerBankAccount.getAccountHolderName(), customerBankAccount.getBankName(),
				customerBankAccount.getCountryCode(), customerBankAccount.getCurrency(), customerBankAccount.getEnabled());
	}

}
