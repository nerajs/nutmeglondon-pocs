package com.nutmeg.client.gocardless.mapper;

import com.gocardless.resources.Payment;
import com.nutmeg.client.gocardless.domain.DirectDebitPayment;

public class PaymentMapper {

	public DirectDebitPayment convertToDirectDebitPayment(
			Payment payment) {
		return new DirectDebitPayment(payment.getId(), payment.getAmount(), payment.getCurrency(), payment.getDescription(), payment.getReference());
	}

}
