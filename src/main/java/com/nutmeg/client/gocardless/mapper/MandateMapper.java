package com.nutmeg.client.gocardless.mapper;

import com.gocardless.resources.Mandate;
import com.nutmeg.client.gocardless.domain.DirectDebitMandate;

public class MandateMapper {

	public DirectDebitMandate convertToDirectDebitMandate(Mandate mandate) {
		return new DirectDebitMandate(mandate.getId(), mandate.getScheme(), mandate.getLinks().getCreditor(), mandate.getLinks().getCustomerBankAccount());
	}
	
	

}
