package com.nutmeg.client.gocardless;

import com.gocardless.GoCardlessClient;
import com.gocardless.resources.Mandate;
import com.gocardless.services.MandateService.MandateCreateRequest;
import com.nutmeg.client.gocardless.domain.DirectDebitMandate;
import com.nutmeg.client.gocardless.mapper.MandateMapper;

public class MandateClient {
	
	private GoCardlessClient client; //TODO most probably this is inappropriate use as client may not be thread safe
	private MandateMapper mandateMapper;

	public MandateClient(GoCardlessClient client, MandateMapper mandateMapper) {
		this.client = client;
		this.mandateMapper = mandateMapper;
	}
	
	
	public DirectDebitMandate create(DirectDebitMandate mandate) {
		
		MandateCreateRequest  mandateCreateRequest = client.mandates().create();
		
		Mandate createdMandate = mandateCreateRequest.withScheme(mandate.getScheme())
				                                     .withLinksCreditor(mandate.getCreditor())
				                                     .withLinksCustomerBankAccount(mandate.getCustomerBankAccount())
				                                     .execute();
		
		return mandateMapper.convertToDirectDebitMandate(createdMandate);
	}


	public DirectDebitMandate getById(String id) {
		Mandate returnedMandate = client.mandates().get(id).execute();
		return mandateMapper.convertToDirectDebitMandate(returnedMandate);
	}
}
