package com.nutmeg.client.gocardless;

import com.gocardless.GoCardlessClient;
import com.gocardless.resources.Customer;
import com.nutmeg.client.gocardless.domain.DirectDebitCustomer;
import com.nutmeg.client.gocardless.mapper.CustomerMapper;

public class CustomerClient {
	
	private GoCardlessClient client; //TODO most probably this is inappropriate use as client may not be thread safe
	private CustomerMapper customerMapper;
	
	public CustomerClient(GoCardlessClient client, CustomerMapper customerMapper) {
		this.client = client;
		this.customerMapper = customerMapper;
	}
	
	public DirectDebitCustomer create(DirectDebitCustomer customer){
		return null;//TODO
	}
	
	public DirectDebitCustomer getById(String id) {
		Customer returnedCustomer = client.customers().get(id).execute();
		return customerMapper.convertToDirectDebitCustomer(returnedCustomer);
	}
	
	
}
