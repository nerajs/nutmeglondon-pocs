package com.nutmeg.client.gocardless;

import com.gocardless.GoCardlessClient;
import com.gocardless.resources.Subscription;
import com.nutmeg.client.gocardless.domain.DirectDebitSubscription;
import com.nutmeg.client.gocardless.mapper.SubscriptionMapper;


public class SubscriptionClient {
	
	private GoCardlessClient client; //TODO most probably this is inappropriate use as client may not be thread safe
	private SubscriptionMapper subscriptionMapper;
	
	public SubscriptionClient(GoCardlessClient client, SubscriptionMapper subscriptionMapper) {
		this.client = client;
		this.subscriptionMapper = subscriptionMapper;
	}
	
	public DirectDebitSubscription create(DirectDebitSubscription subscription){
		//Subscription createdSubscription = client.subscriptions().create().withLinksMandate("MD00007BXKB8PK").withAmount(10000).withCount(5).withIntervalUnit(com.gocardless.services.SubscriptionService.SubscriptionCreateRequest.IntervalUnit.MONTHLY).withCurrency("GBP").withDayOfMonth(7).withName("Gold plan").execute();
		//return subscriptionMapper.convertToDirectDebitSubscription(createdSubscription);
		return null;
	}
	
	public DirectDebitSubscription getById(String id) {
		Subscription returnedSubscription = client.subscriptions().get(id).execute();
		return subscriptionMapper.convertToDirectDebitSubscription(returnedSubscription);
	}
	
	
}
