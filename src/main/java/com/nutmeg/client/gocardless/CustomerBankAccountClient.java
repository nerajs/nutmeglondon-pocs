package com.nutmeg.client.gocardless;

import com.gocardless.GoCardlessClient;
import com.gocardless.resources.CustomerBankAccount;
import com.nutmeg.client.gocardless.domain.DirectDebitCustomerBankAccount;
import com.nutmeg.client.gocardless.mapper.CustomerBankAccountMapper;

public class CustomerBankAccountClient {
	
	

	private GoCardlessClient client; //TODO most probably this is inappropriate use as client may not be thread safe
	private CustomerBankAccountMapper customerBankAccountMapper;
	
	public CustomerBankAccountClient(GoCardlessClient client,
			CustomerBankAccountMapper customerBankAccountMapper) {
		this.client = client;
		this.customerBankAccountMapper = customerBankAccountMapper;
	}
	
	public DirectDebitCustomerBankAccount create(DirectDebitCustomerBankAccount customerBankAccount){
		return null;//TODO
	}
	
	public DirectDebitCustomerBankAccount getById(String id) {
		CustomerBankAccount returnedCustomerBankAccount = client.customerBankAccounts().get(id).execute();
		return customerBankAccountMapper.convertToDirectDebitCustomerBankAccount(returnedCustomerBankAccount);
	}
	


}
