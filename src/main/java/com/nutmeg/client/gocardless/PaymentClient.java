package com.nutmeg.client.gocardless;

import com.gocardless.GoCardlessClient;
import com.gocardless.resources.Payment;
import com.nutmeg.client.gocardless.domain.DirectDebitPayment;
import com.nutmeg.client.gocardless.mapper.PaymentMapper;

public class PaymentClient {
	
	private GoCardlessClient client; //TODO most probably this is inappropriate use as client may not be thread safe
	private PaymentMapper paymentMapper;
	
	public PaymentClient(GoCardlessClient client, PaymentMapper paymentMapper) {
		this.client = client;
		this.paymentMapper = paymentMapper;
	}
	
	public DirectDebitPayment create(DirectDebitPayment payment){
		return null;//TODO
	}
	
	public DirectDebitPayment getById(String id) {
		Payment returnedPayment = client.payments().get(id).execute();
		return paymentMapper.convertToDirectDebitPayment(returnedPayment);
	}
	
	
}
