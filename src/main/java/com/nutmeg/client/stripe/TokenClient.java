package com.nutmeg.client.stripe;

import java.util.Map;

import com.stripe.exception.APIConnectionException;
import com.stripe.exception.APIException;
import com.stripe.exception.AuthenticationException;
import com.stripe.exception.CardException;
import com.stripe.exception.InvalidRequestException;
import com.stripe.model.Token;

public class TokenClient {

	public Token create(Map<String, Object> tokenParams) throws AuthenticationException, InvalidRequestException, APIConnectionException, CardException, APIException {
		return 		Token.create(tokenParams);
	}

}
